import path from "path";
import express from "express";
import fs from "fs";



const JsonLocation="./assests/data.json";
const app= express();

app.get("/",(req,res)=>{
    res.send("<h1>Home Page</h1>");
});

app.get("/customers",(req,res)=>{

    fs.readFile(("./assests/data.json"),"utf-8",(err,data)=>{
        res.send(data);
    
    });
});


app.get('/customers/:customName', (req, res) =>{
    fs.readFile(JsonLocation, 'utf8', (err, data) => {
        let orgData = JSON.parse(data);
        console.log(orgData);
        //req.parms.customName gives the input given by the user in the url
        console.log(req.params.customName);
        let ifFound=false;
        orgData.forEach(JsonElement => {
            
            if(JsonElement["firstName"]==(req.params.customName)){
                res.send(JsonElement); 
                ifFound=true;
            }
        });

        if(!ifFound){
            res.send("<h1>Customer Not Found</h1>");
        }
         
        
    });
});



app.listen(8000,()=>{
    console.log("listing to the port 8000");
});